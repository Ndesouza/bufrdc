use strict;

my $filein=@ARGV[0];
open(IN,"<$filein") or die "$filein: $!";

my $line;
my $count=0;
while (defined ($line = <IN>)) {
	chomp $line;
	$count++;
	$line =~ /( )(\d{6}) (.{64})(.)(.{24}) ([\-\+0-9 ]{3}) ([\-\+0-9 ]{12}) ([\-\+0-9 ]{3})( (.{22}) ([\+\-0-9 ]{4}) ([0-9 ]{9}))?/g or die "$filein wrong format at line $count \n\"$line\"\n";
}
close(IN);

