use strict;

my $filein=@ARGV[0];
open(IN,"<$filein") or die "$filein: $!";

my $line;
my $count=0;
my @list;
my $n=0;
my $c;
while (defined ($line = <IN>)) {
	chomp $line;
	$count++;
	@list = ($line =~ / ([0-9]{6})(([0-9]{3})|( [0-9]{2})|(  [0-9])) ([0-9]{6})(.*)/g); 
	if (@list) {
		if ($count>1 && $n!=$c) {
			my $b=$count-$c;
			die "$filein: wrong number of lines at line $b ($n,$c)\n";
		}
		if (@list[2]) {$n=@list[2];}
		if (@list[3]) {$n=@list[3];}
		if (@list[4]) {$n=@list[4];}
		$c=1;
		next;
	} else {
		@list = ($line =~ / {11}([0-9]{6})(.*)/g);
		if (@list) {
			$c++;
			next;
		}
	}
	die "$filein: bad format at line $count\n\"$line\"\n";
}
close(IN);

