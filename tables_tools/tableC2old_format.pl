use strict;

my $filein=@ARGV[0];
my $fileout=@ARGV[1];
open(IN,"<$filein") or die "$filein: $!";
open(OUT,">$fileout") or die "$fileout: $!";

# check that the input file is in the new table C format
my $line;
my $count=0;
while (defined ($line = <IN>)) {
	chomp $line;
	$count++;
	$line =~ /(.{11}) ([0-9]{8}) ([0-9]{2}) (.*)|(( {22})(.*))/g or die "$fileout wrong format at line $count \n\"$line\"\n";
}
seek(IN,0,0);

# conversion to old table C format
while (defined ($line = <IN>)) {
	chomp $line;
	my $len=length $line;
	if ($line =~ /(.{11}) ([0-9]{8}) ([0-9]{2}) (.*)/g) {
		printf OUT "%s%s%s\n",substr($line,0,13),substr($line,17,$len);
	} else {
		printf OUT "%s%s%s\n",substr($line,0,13),substr($line,17,$len);
	}
}
close(IN);
close(OUT);

# check that the conversion is ok
open(IN,"<$fileout") or die "$fileout: $!";

$count=0;
while (defined ($line = <IN>)) {
	chomp $line;
	$count++;
	$line =~ /((.{11}) ([0-9]{4}) ([0-9]{2}) (.*))|(( {18})(.*))/g or die "$filein wrong format at line $count \n\"$line\"\n";
}

close(IN);

