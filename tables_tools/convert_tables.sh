#!/usr/bin/ksh

set -e

if [ $# -ne 2 ]
then
	echo usage: $0 tables_directory new_tables_directory
	exit 1
fi

dir=$1
newdir=$2
if [ ! -d $newdir ]
then
	mkdir $newdir
fi

for f in $dir/B*.distinct
do
	if [ -f $f ]
	then
		file=`basename $f`
		echo $file
		cp $f $newdir/$file
	fi
done

for f in $dir/C*.distinct
do
	if [ -f $f ]
	then
		file=`basename $f`
		echo $file
		#perl tableCcheck.pl $f 
		perl tableC2new_format.pl $f $newdir/$file
	fi
done

for f in $dir/D*.distinct
do
	if [ -f $f ]
	then
		file=`basename $f`
		echo $file
		cp $f $newdir/$file
	fi
done

if [ -f $dir/links.sh ]
then
	cp $dir/links.sh $newdir/links.sh
	cp $dir/clean.sh $newdir/clean.sh
	cp $dir/Makefile $newdir/Makefile
	cp $dir/Makefile.in $newdir/Makefile.in
	cd $newdir
	./links.sh
fi


