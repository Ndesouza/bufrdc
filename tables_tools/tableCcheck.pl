use strict;

my $filein=@ARGV[0];
open(IN,"<$filein") or die "$filein: $!";

my $line;
my $count=0;
my @list;
my $n=0;
my $c;
my $b=1;
my $nb=1;
my $countb=0;
while (defined ($line = <IN>)) {
	chomp $line;
	$count++;
	@list = ($line =~ /^([0-9]{6}) ([0-9]{4}) ([0-9]{8}) ([0-9]{2}) (.+)/g); 
	if (@list) {
		if ($count>1 && $n!=$c) {
			my $nline=$count-$c-$countb;
			die "$filein: wrong number of lines at line $nline (is $n should be $c)\n";
		}
		$n=int @list[1];
		$nb=int @list[3];
		$countb=0;
		$b=1;
		$c=1;
		next;
	} else {
		@list = ($line =~ /^ {12}([0-9]{8}) ([0-9]{2}) (.*)/g);
		if (@list) {
			$c++;
			if ($nb!=$b) {
				my $nline=$count-$b;
				die "$filein: wrong number of lines at line $nline (is $nb should be $b)\n";
			}
			$nb=int @list[1];
			$b=1;
			next;
		} else {
			@list = ($line =~ /( {22})(.*)/g);
			$countb++;
			$b++;
			next;
		}
	}
	die "$filein: bad format at line $count\n\"$line\"\n";
}
close(IN);

