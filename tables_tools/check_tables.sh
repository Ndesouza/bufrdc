#!/bin/sh

set -e

if [ $# -ne 1 ]
then
	echo usage: $0 tables_directory
	exit 1
fi

toolsdir=`dirname $0`

dir=$1

for f in $dir/B_*.distinct
do
	if [ -f $f ]
	then
		perl $toolsdir/tableBcheck.pl $f
	fi
done

for f in $dir/C_*.distinct
do
	if [ -f $f ]
	then
		perl $toolsdir/tableCcheck.pl $f 
	fi
done

for f in $dir/D_*.distinct
do
	if [ -f $f ]
	then
		perl $toolsdir/tableDcheck.pl $f 
	fi
done


