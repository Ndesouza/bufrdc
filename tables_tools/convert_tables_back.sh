#!/usr/bin/ksh

set -e

dir=../bufrtables
newdir=../bufrtables.oldformat
if [ ! -d $newdir ]
then
	mkdir $newdir
fi

wdir=`pwd`
cd $dir
./clean.sh

cd $wdir

for f in $dir/B*
do
	if [ -f $f ]
	then
		file=`basename $f`
		echo $file
		cp $f $newdir/$file
	fi
done

for f in $dir/C*
do
	if [ -f $f ]
	then
		file=`basename $f`
		echo $file
		perl tableC2old_format.pl $f $newdir/$file
	fi
done

for f in $dir/D*
do
	if [ -f $f ]
	then
		file=`basename $f`
		echo $file
		cp $f $newdir/$file
	fi
done

if [ -f $dir/links.sh ]
then
	cp $dir/links.sh $newdir/links.sh
	cp $dir/clean.sh $newdir/clean.sh
	cd $newdir
	./links.sh
fi

cd $wdir
cd $dir
./links.sh

