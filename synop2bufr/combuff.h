C Copyright 1981-2012 ECMWF. 
C
C This software is licensed under the terms of the GNU Lesser 
C General Public License Version 3 which can be obtained at 
C http://www.gnu.org/licenses/lgpl.html.  
C 
C In applying this licence, ECMWF does not waive the privileges 
C and immunities granted to it by virtue of its status as an 
C intergovernmental organisation nor does it submit to any
C jurisdiction. 
C 
C
C     'COMBUFF' CONTAINS READ IN AREAS USED BY VARIOUS FILES ,
C     AS WELL AS END-OF-FILE INDICATOR.
C
      COMMON / COMBUFF / IEOF,IOPTS(678),IPARAMS(39000),IPOINTS(128)
     C                   ,IMPSTA(2000)
C
C
