C Copyright 1981-2012 ECMWF. 
C
C This software is licensed under the terms of the GNU Lesser 
C General Public License Version 3 which can be obtained at 
C http://www.gnu.org/licenses/lgpl.html.  
C 
C In applying this licence, ECMWF does not waive the privileges 
C and immunities granted to it by virtue of its status as an 
C intergovernmental organisation nor does it submit to any
C jurisdiction. 
C 
C
C     'COMERROR' contains parameters need for manual error correction 
C
      COMMON / COMERROR / Nmode 
C
C                         Nmode=0  - Normal decoding
C                              =1  - Manual correction
