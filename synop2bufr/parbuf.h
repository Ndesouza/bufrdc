C Copyright 1981-2012 ECMWF. 
C
C This software is licensed under the terms of the GNU Lesser 
C General Public License Version 3 which can be obtained at 
C http://www.gnu.org/licenses/lgpl.html.  
C 
C In applying this licence, ECMWF does not waive the privileges 
C and immunities granted to it by virtue of its status as an 
C intergovernmental organisation nor does it submit to any
C jurisdiction. 
C 

C
      PARAMETER(JSUP =   9,JSEC0=   3,JSEC1= 40,JSEC2=4096 ,JSEC3=   4,
     1       JSEC4=   2,JELEM=160000,JSUBS=400,JCVAL=550 ,JBUFL=128000,
     2       JBPW =  64,JTAB=3000,JCTAB=120,JCTST=1800,JCTEXT= 200,
     3       JWORK=4096000,JKEY=46)
C
