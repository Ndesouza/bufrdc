C Copyright 1981-2012 ECMWF. 
C
C This software is licensed under the terms of the GNU Lesser 
C General Public License Version 3 which can be obtained at 
C http://www.gnu.org/licenses/lgpl.html.  
C 
C In applying this licence, ECMWF does not waive the privileges 
C and immunities granted to it by virtue of its status as an 
C intergovernmental organisation nor does it submit to any
C jurisdiction. 
C 
      PARAMETER (JP1 =  100,JP2 =  3000,JP3 = 4096,JP4 =  678,JP5 =60,
     1           JP6 =   50,JP7 =  200,JP8 = 100,JP9  =  200,JP10= 200,
     2           JP11= 3600,JP12=    3,JP13=   26,JP14=   32,
     3           jp15=512000,jp17=20480,jp18=    8,jp19= 2000,jp20=128,
     4           JP21=    2,JP22=3600) 
C
