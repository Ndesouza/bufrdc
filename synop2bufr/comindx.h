C Copyright 1981-2012 ECMWF. 
C
C This software is licensed under the terms of the GNU Lesser 
C General Public License Version 3 which can be obtained at 
C http://www.gnu.org/licenses/lgpl.html.  
C 
C In applying this licence, ECMWF does not waive the privileges 
C and immunities granted to it by virtue of its status as an 
C intergovernmental organisation nor does it submit to any
C jurisdiction. 
C 
C
C  'COMINDX' CONTAINS POINTERS TO BEGINNING AND END OF STARTING
C     LINE (ISL & JSL)  , ABBREVIATED HEADING LINE (IAH & JAH)  ,
C     'MIMIMJMJ' LINE (IMI & JMI)  , END OF REPORT (IEQ)  , AND
C     END OF BULLETIN (IGS)  ,  AND A WORKING POINTERS (IPT)
C     AND (KPT) AND BEGINNING OF THE FIRST LEVEL (LPT).
C     OBS...  IN CASE OF TEMP SHIP REPORT WHEN SHIP'S NAME IS ON
C             SEPARATE LINE POINTERS IMI & JMI POINT TO THE THIS LINE
C
      COMMON / COMINDX / ISL,JSL,IAH,JAH,IMI,JMI,IEQ,IGS,IPT,KPT
     C                  ,LPT,I1
C
