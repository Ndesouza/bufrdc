C Copyright 1981-2012 ECMWF. 
C
C This software is licensed under the terms of the GNU Lesser 
C General Public License Version 3 which can be obtained at 
C http://www.gnu.org/licenses/lgpl.html.  
C 
C In applying this licence, ECMWF does not waive the privileges 
C and immunities granted to it by virtue of its status as an 
C intergovernmental organisation nor does it submit to any
C jurisdiction. 
C 
C
      COMMON /COMPOIN/ NBPW,NWPT,NBPT,OPS2,
     1                 MASKS(JP14),MBUF(JP3)
C
C     NBPW          -  NUMBER OF BITS PER COMPUTER WORD
C     NWPT          -  POINTER TO WORD IN ARRAY MBUFF
C     NBPT          -  POINTER TO BIT INSIDE AN COMPUTER WORD
C     MASKS         -  ARRAY CONTAINING BIT MASKS
C     MINDIC        -  MISSING DATA INDICATOR
C     MBUF          -  ARRAY CONTAINING SINGLE BUFR MESSAGE
C
