#!/bin/sh
set -e

path=`pwd`


BUFR_TABLES=$path/bufrtables/
export BUFR_TABLES

examples/bufr_decode_all -i data/temp_101.bufr | sed '/bufrtables/ d' > test.log
examples/bufr_decode_all -i data/syno_1.bufr | sed '/bufrtables/ d' >> test.log
examples/bufr_decode_all -i data/airc_142.bufr | sed '/bufrtables/ d' >> test.log

set +e

diff test.log test.log.good
if [ $? != 0 ]
then
	echo "****************************************"
	echo "****************************************"
	echo TEST $0 FAILED              
	echo "****************************************"
	echo "****************************************"
	exit 1
fi

set +e
for f in data/*.bufr
do
	echo examples/bufr_decode_all -i $f
	examples/bufr_decode_all -i $f > /dev/null


	if [ $? != 0 ]
	then
		echo "****************************************"
		echo "****************************************"
		echo TEST $0 FAILED              
		echo "****************************************"
		echo "****************************************"
		exit 1
	fi

done
