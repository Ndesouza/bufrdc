#                                Makefile for libbufrex
#
R64 = R64
TARGETS = all clean
LIBRARY = libbufr$(R64).a
SHELL=/bin/sh
SUBDIRS = fortranC bufrdc pbio bufrtables examples synop2bufr

all :
	@for name in $(SUBDIRS); do\
	( echo "*************************"; \
	  echo "*** Make in $$name "; \
	  echo "*************************"; \
	cd $$name ; make ; ) done
	./test.sh
	tables_tools/check_tables.sh bufrtables

clean   :
	@for name in $(SUBDIRS); do\
	( echo "*** Clean in $$name ***" ;cd $$name ; make clean ); \
	done
	rm -f $(LIBRARY)
