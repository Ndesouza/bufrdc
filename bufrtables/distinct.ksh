#!/usr/bin/ksh

set -e

for f in *.TXT
do
		if [ ! -L $f ]
		then
				t=`echo $f | awk 'BEGIN{FS="";}{print $1;}'`
				df=${t}_`md5sum $f | awk '{print $1;}' `.distinct
				if [ ! -f $df ]
				then
						cp $f $df
				fi
		fi
done

cat /dev/null > links.sh
for f in *.TXT
do
		t=`echo $f | awk 'BEGIN{FS="";}{print $1;}'`
		df=${t}_`md5sum $f | awk '{print $1;}' `.distinct
		rm -f $f
		ln -s $df $f
		echo ln -s $df $f >> links.sh
done

chmod +x links.sh
