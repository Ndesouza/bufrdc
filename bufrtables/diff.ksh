set -e

newdir=/var/tmp/mac/p4/bufrdb/data/bufrtables_from_DB/000400/
for f in $newdir/*TXT
do
		b=`basename $f`
		echo === processing $b
		if [ -f $b ] 
		then
				set +e
				diff $b $f
				if [ $? != 0 ]
				then
						xxdiff $b $f
				fi
		else
				echo $b is new
		fi
done
